= local-openid: Single User, Ephemeral OpenID Provider

local-openid allows users with shell accounts on servers to authenticate
with OpenID consumers by editing a YAML file in their home directory
instead of authenticating through HTTP/HTTPS.

1. Encounter a login page that accepts OpenID (the consumer)
2. Login into your own server (if you're not already logged in)
3. Start the local-openid app on your server
4. Login using your OpenID (on the consumer)
   - you should be redirected to your local-openid application
5. edit ~/.local-openid/config.yml on your server to approve the consumer
   This config file is only created after the consumer makes a successful
   request to your server.
6. Reload the local-openid page your browser was on.
   - you should be logged in to the OpenID consumer site
   - If not, check the error log (usually stderr) of local-openid
8. Shut down the local-openid application.

== local-openid exists for the following reasons:

1. Passwords and password managers feel clumsy to me on web browsers.
   On the other hand; using ssh, editing text files, and running servers
   are second nature.  Clearly, local-openid is not for everyone.
2. Identity providers may not last.  Companies die and business plans
   change.  I'd rather my online identity not be subject to those whims.
3. OpenID providers could be compromised without disclosure.  With
   local-openid, I have server logs to know if somebody is even trying
   something fishy with my identity.  The vector for compromising my
   identity is greatly reduced because my local-openid instance has 99.999%
   downtime.

== Install

The following command should install local-openid and all dependencies:

  gem install local-openid

setup.rb is also provided for non-Rubygems users.

== Requirements

local-openid is a small Sinatra application.  It requires the Ruby
OpenID library (2.x), Sinatra (1.0), Rack and any Rack-enabled
server.  To be useful, it also depends on having a user account on a
machine with a publically-accessible IP and DNS name to use as your
OpenID identity.

== Running

"local-openid" should be installed in your $PATH by RubyGems or
setup.rb.  It is a Sinatra application and takes the usual
command-line arguments.  It binds on all addresses (0.0.0.0) and port
4567 by default, using the standard WEBrick web server.

You may specify a different port with the *-p* switch and address with
the *-o* switch.  The following command will start local-openid on port
3000 bound to localhost (useful if behind a reverse proxy like nginx).

  local-openid -o 127.0.0.1 -p 3000

== Hacking

I don't have any plans for more development with local-openid.  It was
after all, just a weekend hack.  It does what I want it to and nothing
more.

You can use the {mailing list}[mailto:local-openid@public-inbox.org] to
share ideas, patches, pull requests with other users.  Remember, I
wrote local-openid because I find the web difficult to use.  So I'll
only accept communication about local-openid via email :)

Feel free to fork it and customize it to your needs.  Of course, drop me
a line if you fix any bugs or notice any security holes in it.

You can get the latest source via git from the following locations:

  git://yhbt.net/local-openid.git
  https://yhbt.net/local-openid.git
  git://repo.or.cz/local-openid.git (mirror)
  http://repo.or.cz/r/local-openid.git (mirror)

You may browse the code from the web and download the latest tarballs here:

* https://yhbt.net/local-openid.git
* http://repo.or.cz/w/local-openid.git (gitweb mirror)

== Disclaimer

There is NO WARRANTY whatsoever, implied or otherwise.  OpenID may not
be the best choice for dealing with security-sensitive data, and this
application is just a weekend hack with no real security auditing.  On
the other hand, it's quite hard for somebody to steal your OpenID
credentials when your provider implementation has 99.999% downtime :)

== Contact

* Original author: Eric Wong, e@80x24.org
* mailing list: local-openid@public-inbox.org
