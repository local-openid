manifest = File.exist?('.manifest') ?
  IO.readlines('.manifest').map!(&:chomp!) : `git ls-files`.split("\n")

Gem::Specification.new do |s|
  s.name = %q{local-openid}
  s.version = (ENV['VERSION'] || '0.4.2').dup
  s.authors = ["Eric Wong"]
  s.description = File.read('README').split("\n\n")[1]
  s.email = %q{e@80x24.org}
  s.executables = %w(local-openid)
  s.extra_rdoc_files = IO.readlines('.document').map!(&:chomp!).keep_if do |f|
    File.exist?(f)
  end
  s.files = manifest
  s.homepage = 'https://yhbt.net/local-openid'
  s.summary = 'Single User, Ephemeral OpenID Provider'
  s.add_dependency(%q<rack>, ["~> 1.3"])
  s.add_dependency(%q<sinatra>, ["~> 1.0"])
  s.add_dependency(%q<ruby-openid>, ["~> 2.1.7"])
  s.licenses = %w(AGPL-3.0+)
end
